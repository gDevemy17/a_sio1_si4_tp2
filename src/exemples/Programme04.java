package exemples;

import java.util.Random;
import java.util.Scanner;

public class Programme04 {

    public static void main(String[] args) {
       
        Scanner  clavier=new Scanner(System.in);
        
        int nbADeviner;
        int nbPropose;
      
        Random rd=new Random();
        
        nbADeviner=rd.nextInt(10)+1;  // rd.nextInt(10) fornit au hasard un nombre entre 0 et 9 
                                      // nbADeviner=rd.nextInt(10)+1 sera donc compris entre 1 et 10 
        
        System.out.println("Proposez un nombre entre 1 et 10");
        nbPropose=clavier.nextInt();   // utilisateur propose un nombre entre 1 et 10
        
        if(nbPropose == nbADeviner){   // on teste s'il y a égalité entre le nombre généré par le programme 
                                       // et le nombre saisi par l'utilisateur
        
            System.out.println("Vous avez gagné!\n");  // ici on traite le cas ou il y a égalité
        }
        else{ // ici on traite le cas contraire
        
            System.out.println("Vous avez perdu!");
            System.out.println("Le nombre à deviner était: "+ nbADeviner+"\n");
        }          
    }
}

